<?php

require_once('animal.php');
require_once('frog.php');
require_once('ape.php');

$sheep = new animal("shaun");
echo "Name : " . $sheep->name;
echo "<br>";
echo "Legs : " . $sheep->legs;
echo "<br>";
echo "Cold Blooded : " .$sheep->cold_blooded;
echo "<br><br>";

$kodok = new frog("buduk");
echo "Name : " . $kodok->type;
echo "<br>";
echo "Legs : " . $kodok->legs;
echo "<br>";
echo "Cold Blooded : " .$kodok->cold_blooded;
echo "<br>";
echo "Jump : " . $kodok->jump();
echo "<br><br>";

$sungokong = new ape("kera sakti");
echo "Name : " . $sungokong->type;
echo "<br>";
echo "Legs : " . $sungokong->legs;
echo "<br>";
echo "Cold Blooded : " .$sungokong->cold_blooded;
echo "<br>";
echo "Yell : " . $sungokong->yell();
echo "<br><br>";
?>
